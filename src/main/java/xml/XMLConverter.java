package xml;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import db.model.core.ITable;

import javax.xml.bind.*;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

public class XMLConverter {

    @Nullable
    public static <T extends ITable> String jaxbObjectToXML(@NotNull T t) throws JAXBException {
        if (t == null){
            return "";
        }
        final JAXBContext context = JAXBContext.newInstance(t.getClass());
        final Marshaller marshaller = context.createMarshaller();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        final StringWriter stringWriter = new StringWriter();

        marshaller.marshal(t, stringWriter);

        return stringWriter.toString();

    }

    @Nullable
    public static <T extends ITable> T jaxbXMLToObject(@NotNull Class<T> tClass, @NotNull String xml)
            throws JAXBException {

        final JAXBContext context = JAXBContext.newInstance(tClass);
        final Unmarshaller un = context.createUnmarshaller();
        final StringReader stringReader = new StringReader(xml);

        return (T) un.unmarshal(stringReader);
    }

    @Nullable
    public static <T extends ITable> String jaxbListToXML(@NotNull List<T> list) throws JAXBException{

        if (list.isEmpty()){
            return "";
        }

        final JAXBContext context = JAXBContext.newInstance(Wrapper.class, list.get(0).getClass());
        final Marshaller marshaller = context.createMarshaller();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        final StringWriter stringWriter = new StringWriter();

        final Wrapper<T> wrapper = new Wrapper<>(list);
        final JAXBElement<Wrapper> jaxbElement = new JAXBElement<>(new QName("list"), Wrapper.class, wrapper);
        marshaller.marshal(jaxbElement, stringWriter);

        return stringWriter.toString();
    }

    @Nullable
    public static <T extends ITable> List<T> jaxbXMLToList(@NotNull Class<T> tClass, @NotNull String xml)
            throws JAXBException {

        final JAXBContext context = JAXBContext.newInstance(Wrapper.class, tClass);
        final Unmarshaller un = context.createUnmarshaller();
        final StringReader stringReader = new StringReader(xml);
        final StreamSource streamSource = new StreamSource(stringReader);

        return ((Wrapper<T>) un.unmarshal(streamSource, Wrapper.class).getValue()).getList();
    }
}
