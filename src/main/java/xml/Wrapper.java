package xml;

import db.model.core.ITable;

import javax.xml.bind.annotation.XmlAnyElement;
import java.util.List;

public class Wrapper<T extends ITable> {

    private List<T> list;

    public Wrapper(List<T> list) {
        this.list = list;
    }

    public Wrapper() {}

    @XmlAnyElement(lax = true)
    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
