package soap;

import db.model.ImageType;
import db.service.ImageTypeService;
import soap.core.IImageTypeService;
import xml.XMLConverter;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.sql.SQLException;

@WebService(endpointInterface = "soap.core.IImageTypeService")
public class SOAPImageType implements IImageTypeService {


    @Override
    public String getAll() {
        try {
            return XMLConverter.jaxbListToXML(new ImageTypeService().getAll());
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String get(String id) {
        try {
            return XMLConverter.jaxbObjectToXML(new ImageTypeService().get(id));
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int add(ImageType t) {
        try {
            return new ImageTypeService().add(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int edit(ImageType t) {
        try {
            return new ImageTypeService().edit(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int delete(ImageType t) {
        try {
            return new ImageTypeService().delete(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
