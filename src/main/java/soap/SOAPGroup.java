package soap;

import db.model.Group;
import db.service.GroupService;
import soap.core.IGroupService;
import xml.XMLConverter;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.sql.SQLException;

@WebService(endpointInterface = "soap.core.IGroupService")
public class SOAPGroup implements IGroupService {


    @Override
    public String getAll() {
        try {
            return XMLConverter.jaxbListToXML(new GroupService().getAll());
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String get(String id) {
        try {
            return XMLConverter.jaxbObjectToXML(new GroupService().get(id));
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int add(Group t) {
        try {
            return new GroupService().add(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int edit(Group t) {
        try {
            return new GroupService().edit(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int delete(Group t) {
        try {
            return new GroupService().delete(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
