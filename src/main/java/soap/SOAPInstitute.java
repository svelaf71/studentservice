package soap;

import db.model.Institute;
import db.service.InstituteService;
import soap.core.IInsituteService;
import xml.XMLConverter;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.sql.SQLException;

@WebService(endpointInterface = "soap.core.IInsituteService")
public class SOAPInstitute implements IInsituteService {


    @Override
    public String getAll() {
        try {
            return XMLConverter.jaxbListToXML(new InstituteService().getAll());
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String get(String id) {
        try {
            return XMLConverter.jaxbObjectToXML(new InstituteService().get(id));
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int add(Institute t) {
        try {
            return new InstituteService().add(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int edit(Institute t) {
        try {
            return new InstituteService().edit(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int delete(Institute t) {
        try {
            return new InstituteService().delete(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
