package soap;

import db.model.Form;
import db.service.FormService;
import soap.core.IFormService;
import xml.XMLConverter;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.sql.SQLException;

@WebService(endpointInterface = "soap.core.IFormService")
public class SOAPForm implements IFormService {


    @Override
    public String getAll() {
        try {
            return XMLConverter.jaxbListToXML(new FormService().getAll());
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String get(String id) {
        try {
            return XMLConverter.jaxbObjectToXML(new FormService().get(id));
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int add(Form t) {
        try {
            return new FormService().add(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int edit(Form t) {
        try {
            return new FormService().edit(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int delete(Form t) {
        try {
            return new FormService().delete(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
