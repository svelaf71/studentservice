package soap.core;

import db.model.Form;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IFormService {

    @WebMethod
    String getAll();

    @WebMethod
    String get(String id);

    @WebMethod
    int add(Form t);

    @WebMethod
    int edit(Form t);

    @WebMethod
    int delete(Form t);

}
