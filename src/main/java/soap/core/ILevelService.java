package soap.core;

import db.model.Level;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ILevelService {

    @WebMethod
    String getAll();

    @WebMethod
    String get(String id);

    @WebMethod
    int add(Level t);

    @WebMethod
    int edit(Level t);

    @WebMethod
    int delete(Level t);

}
