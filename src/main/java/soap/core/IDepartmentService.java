package soap.core;

import db.model.Department;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IDepartmentService {

    @WebMethod
    String getAll();

    @WebMethod
    String get(String id);

    @WebMethod
    int add(Department t);

    @WebMethod
    int edit(Department t);

    @WebMethod
    int delete(Department t);

}
