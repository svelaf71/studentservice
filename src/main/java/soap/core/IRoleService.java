package soap.core;

import db.model.Role;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IRoleService {

    @WebMethod
    String getAll();

    @WebMethod
    String get(String id);

    @WebMethod
    int add(Role t);

    @WebMethod
    int edit(Role t);

    @WebMethod
    int delete(Role t);

}
