package soap.core;

import db.model.CuratorTeacher;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ICuratorTeacherService {

    @WebMethod
    String getAll();

    @WebMethod
    String get(String id);

    @WebMethod
    int add(CuratorTeacher t);

    @WebMethod
    int edit(CuratorTeacher t);

    @WebMethod
    int delete(CuratorTeacher t);

}
