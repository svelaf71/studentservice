package soap.core;

import db.model.Group;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IGroupService {

    @WebMethod
    String getAll();

    @WebMethod
    String get(String id);

    @WebMethod
    int add(Group t);

    @WebMethod
    int edit(Group t);

    @WebMethod
    int delete(Group t);

}
