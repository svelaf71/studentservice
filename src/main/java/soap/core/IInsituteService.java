package soap.core;

import db.model.Institute;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IInsituteService {

    @WebMethod
    String getAll();

    @WebMethod
    String get(String id);

    @WebMethod
    int add(Institute t);

    @WebMethod
    int edit(Institute t);

    @WebMethod
    int delete(Institute t);

}
