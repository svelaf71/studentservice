package soap.core;

import db.model.Grade;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IGradeService {

    @WebMethod
    String getAll();

    @WebMethod
    String get(String id);

    @WebMethod
    int add(Grade t);

    @WebMethod
    int edit(Grade t);

    @WebMethod
    int delete(Grade t);

}
