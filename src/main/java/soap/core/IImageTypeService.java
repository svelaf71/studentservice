package soap.core;

import db.model.ImageType;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IImageTypeService {

    @WebMethod
    String getAll();

    @WebMethod
    String get(String id);

    @WebMethod
    int add(ImageType t);

    @WebMethod
    int edit(ImageType t);

    @WebMethod
    int delete(ImageType t);

}
