package soap;

import db.model.Level;
import db.service.LevelService;
import soap.core.ILevelService;
import xml.XMLConverter;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.sql.SQLException;

@WebService(endpointInterface = "soap.core.ILevelService")
public class SOAPLevel implements ILevelService {

    @Override
    public String getAll() {
        try {
            return XMLConverter.jaxbListToXML(new LevelService().getAll());
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String get(String id) {
        try {
            return XMLConverter.jaxbObjectToXML(new LevelService().get(id));
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int add(Level role) {
        try {
            return new LevelService().add(role);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int edit(Level role) {
        try {
            return new LevelService().edit(role);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int delete(Level role) {
        try {
            return new LevelService().delete(role);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
