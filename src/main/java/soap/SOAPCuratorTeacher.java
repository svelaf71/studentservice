package soap;

import db.model.CuratorTeacher;
import db.service.CuratorTeacherService;
import soap.core.ICuratorTeacherService;
import xml.XMLConverter;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.sql.SQLException;

@WebService(endpointInterface = "soap.core.ICuratorTeacherService")
public class SOAPCuratorTeacher implements ICuratorTeacherService {


    @Override
    public String getAll() {
        try {
            return XMLConverter.jaxbListToXML(new CuratorTeacherService().getAll());
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String get(String id) {
        try {
            return XMLConverter.jaxbObjectToXML(new CuratorTeacherService().get(id));
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int add(CuratorTeacher t) {
        try {
            return new CuratorTeacherService().add(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int edit(CuratorTeacher t) {
        try {
            return new CuratorTeacherService().edit(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int delete(CuratorTeacher t) {
        try {
            return new CuratorTeacherService().delete(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
