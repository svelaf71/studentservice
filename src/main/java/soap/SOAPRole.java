package soap;

import db.model.Role;
import db.service.RoleService;
import soap.core.IRoleService;
import xml.XMLConverter;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.sql.SQLException;

@WebService(endpointInterface = "soap.core.IRoleService")
public class SOAPRole implements IRoleService {

    @Override
    public String getAll() {
        try {
            return XMLConverter.jaxbListToXML(new RoleService().getAll());
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String get(String id) {
        try {
            return XMLConverter.jaxbObjectToXML(new RoleService().get(id));
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int add(Role role) {
        try {
            return new RoleService().add(role);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int edit(Role role) {
        try {
            return new RoleService().edit(role);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int delete(Role role) {
        try {
            return new RoleService().delete(role);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
