package soap;

import db.model.Grade;
import db.service.GradeService;
import soap.core.IGradeService;
import xml.XMLConverter;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.sql.SQLException;

@WebService(endpointInterface = "soap.core.IGradeService")
public class SOAPGrade implements IGradeService {


    @Override
    public String getAll() {
        try {
            return XMLConverter.jaxbListToXML(new GradeService().getAll());
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String get(String id) {
        try {
            return XMLConverter.jaxbObjectToXML(new GradeService().get(id));
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int add(Grade t) {
        try {
            return new GradeService().add(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int edit(Grade t) {
        try {
            return new GradeService().edit(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int delete(Grade t) {
        try {
            return new GradeService().delete(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
