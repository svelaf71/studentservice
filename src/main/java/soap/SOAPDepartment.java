package soap;

import db.model.Department;
import db.service.DepartmentService;
import soap.core.IDepartmentService;
import xml.XMLConverter;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.sql.SQLException;

@WebService(endpointInterface = "soap.core.IDepartmentService")
public class SOAPDepartment implements IDepartmentService {


    @Override
    public String getAll() {
        try {
            return XMLConverter.jaxbListToXML(new DepartmentService().getAll());
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String get(String id) {
        try {
            return XMLConverter.jaxbObjectToXML(new DepartmentService().get(id));
        } catch (JAXBException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int add(Department t) {
        try {
            return new DepartmentService().add(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int edit(Department t) {
        try {
            return new DepartmentService().edit(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int delete(Department t) {
        try {
            return new DepartmentService().delete(t);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
