package db.service;

import db.model.Level;
import db.model.core.ITable;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class LevelService extends AService<Level> {

    @Override
    public Level get(String id) throws SQLException {
        return get(Level.TABLE_NAME, id);
    }

    @Override
    public List<Level> getAll() throws SQLException {
        return getAll(Level.TABLE_NAME);
    }

    @Override
    public List<Level> parseResult(ResultSet resultSet) throws SQLException {
        final List<Level> levels = new ArrayList<>();

        try {
            while (resultSet.next()){
                final String id = resultSet.getString(ITable.ID);
                final int levelName = resultSet.getInt(Level.LEVEL_NAME);

                final Timestamp created = resultSet.getTimestamp(ITable.CREATED);
                final Timestamp modified = resultSet.getTimestamp(ITable.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(ITable.DELETED);

                levels.add(new Level(id, levelName, created, modified, deleted));
            }
        }finally {
            resultSet.close();
        }

        return levels;
    }
}
