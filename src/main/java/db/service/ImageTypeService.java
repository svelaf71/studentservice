package db.service;

import com.sun.istack.internal.NotNull;
import db.model.ImageType;
import db.model.core.ITable;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ImageTypeService extends AService<ImageType> {

    @Override
    public ImageType get(@NotNull String id) throws SQLException {
        return get(ImageType.TABLE_NAME, id);
    }

    @Override
    public List<ImageType> getAll() throws SQLException {
        return getAll(ImageType.TABLE_NAME);
    }

    @Override
    public List<ImageType> parseResult(ResultSet resultSet) throws SQLException {
        final List<ImageType> imageTypes = new ArrayList<>();

        try {
            while (resultSet.next()){
                final String id = resultSet.getString(ITable.ID);
                final String imageType = resultSet.getString(ImageType.IMAGE_TYPE);

                final Timestamp created = resultSet.getTimestamp(ITable.CREATED);
                final Timestamp modified = resultSet.getTimestamp(ITable.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(ITable.DELETED);

                imageTypes.add(new ImageType(id, imageType, created, modified, deleted));
            }
        }finally {
            resultSet.close();
        }


        return imageTypes;
    }
}
