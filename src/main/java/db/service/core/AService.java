package db.service.core;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import db.model.core.ITable;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public abstract class AService<T extends ITable> implements IService<T>{

    @Nullable
    protected T get(@NotNull String tableName, @NotNull String id) throws SQLException {
        Statement statement = null;

        try{
            statement = getConnection().createStatement();

            final List<T> result = parseResult(statement.executeQuery("SELECT * FROM " + tableName +
                    " WHERE " + ITable.ID + " = '" + id + "' AND " + ITable.DELETED + " IS NULL" + ";"));

            return result != null && !result.isEmpty() ? result.get(0) : null;
        }finally {

            if (statement != null) {
                statement.close();
            }
        }

    }

    protected List<T> getAll(@NotNull String tableName) throws SQLException {
        Statement statement = null;

        try{
            statement = getConnection().createStatement();

            return parseResult(statement.executeQuery("SELECT * FROM " + tableName +
                    " WHERE " + ITable.DELETED + " IS NULL" + ";"));
        }finally {

            if (statement != null) {
                statement.close();
            }
        }
    }
}
