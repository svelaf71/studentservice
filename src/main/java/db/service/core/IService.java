package db.service.core;


import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import db.model.core.ITable;
import org.apache.commons.codec.digest.DigestUtils;

import java.sql.*;
import java.util.List;
import java.util.Random;

public interface IService<T extends ITable> {
    /**
     * SELECT * FROM TABLE_NAME WHERE id = (@param id);
     */
    T get(@NotNull String id) throws SQLException;

    /**
     * SELECT * FROM TABLE_NAME;
     */
    List<T> getAll() throws SQLException;

    /**
     * INSERT INTO TABLE_NAME VALUES(...);
     */
    default int add(@NotNull T t) throws SQLException {

        Statement statement = null;
        t.setId(generateId());

        try {
            statement = getConnection().createStatement();

            return statement.executeUpdate("INSERT INTO " + t.getTableName() + " " + t.insertQuery() + ";");
        }finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

    /**
     * UPDATE TABLE_NAME SET ... WHERE id = t.getId();
     */
    default int edit(@NotNull T t) throws SQLException {

        Statement statement = null;

        try {
            statement = getConnection().createStatement();

            return statement.executeUpdate("UPDATE " + t.getTableName() + " SET " + t.updateQuery() +  ", " +
                    T.MODIFIED + " = '" + new Timestamp(System.currentTimeMillis()) +
                    "' WHERE " + T.ID + " = '" + t.getId() + "';");
        }finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

    /**
     * DELETE FROM TABLE_NAME WHERE id = (t.getId);
     */
    default int delete(@NotNull T t) throws SQLException {

        Statement statement = null;

        try {
            statement = getConnection().createStatement();

            return statement.executeUpdate("UPDATE " + t.getTableName() + " SET " + T.DELETED +
                    " = '" + new Timestamp(System.currentTimeMillis()) + "' WHERE " + T.ID + " = '" + t.getId() + "';");
        }finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

    /**
     * parse result query;
     */
    @Nullable
    List<T> parseResult(ResultSet resultSet) throws SQLException;

    default Connection getConnection(){
        return DBConnection.getINSTANCE().getConnection();
    }

    @NotNull
    static String generateId(){
        return DigestUtils.md2Hex(String.valueOf(System.currentTimeMillis() * new Random().nextDouble()));
    }
}
