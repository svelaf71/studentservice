package db.service.core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class DBConnection {
    private static final String TAG = DBConnection.class.getSimpleName();

    private static final String DB_NAME = "student_db";
    private static final String URL = "jdbc:mysql://localhost:3306/" + DB_NAME;

    private static final String USER_NAME = "root";
    private static final String PASSWORD = "aruzem39";

    private Connection connection;

    private static final DBConnection INSTANCE = new DBConnection();

    private DBConnection() {
        createConnection();
    }

    private void createConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
        } catch (ClassNotFoundException e) {
            System.out.println(TAG + "JDBC Driver not found");
            connection = null;
        } catch (SQLException e) {
            System.out.println(TAG + "Invalid url, user name or password");
            connection = null;
        }
    }

    Connection getConnection() {
        return connection;
    }

    static DBConnection getINSTANCE() {
        return INSTANCE;
    }
}
