package db.service;

import com.sun.istack.internal.NotNull;
import db.model.Form;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class FormService extends AService<Form>  {

    @Override
    public Form get(@NotNull String id) throws SQLException {
        return get(Form.TABLE_NAME, id);
    }

    @Override
    public List<Form> getAll() throws SQLException {
        return getAll(Form.TABLE_NAME);
    }

    @Override
    public List<Form> parseResult(ResultSet resultSet) throws SQLException {
        final List<Form> forms = new ArrayList<>();

        try {
            while (resultSet.next()) {
                final String id = resultSet.getString(Form.ID);
                final String formName = resultSet.getString(Form.FORM_NAME);

                final Timestamp created = resultSet.getTimestamp(Form.CREATED);
                final Timestamp modified = resultSet.getTimestamp(Form.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(Form.DELETED);

                final Form form = new Form(id, formName, created, modified, deleted);

                forms.add(form);
            }
        }finally {
            resultSet.close();
        }

        return forms;
    }
}
