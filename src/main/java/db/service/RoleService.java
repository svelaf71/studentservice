package db.service;

import db.model.Role;
import db.model.core.ITable;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class RoleService extends AService<Role> {

    @Override
    public Role get(String id) throws SQLException {
        return get(Role.TABLE_NAME, id);
    }

    @Override
    public List<Role> getAll() throws SQLException {
        return getAll(Role.TABLE_NAME);
    }

    @Override
    public List<Role> parseResult(ResultSet resultSet) throws SQLException {
        final List<Role> roles = new ArrayList<>();

        try {
            while (resultSet.next()){
                final String id = resultSet.getString(ITable.ID);
                final String roleName = resultSet.getString(Role.ROLE_NAME);

                final Timestamp created = resultSet.getTimestamp(ITable.CREATED);
                final Timestamp modified = resultSet.getTimestamp(ITable.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(ITable.DELETED);

                roles.add(new Role(id, roleName, created, modified, deleted));
            }
        }finally {
            resultSet.close();
        }

        return roles;
    }
}
