package db.service;

import com.sun.istack.internal.NotNull;
import db.model.ImageType;
import db.model.StudentPhoto;
import db.service.core.AService;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class StudentPhotoService extends AService<StudentPhoto> {
    @Override
    public StudentPhoto get(@NotNull String id) throws SQLException {
        return get(StudentPhoto.TABLE_NAME, id);
    }

    @Override
    public List<StudentPhoto> getAll() throws SQLException {
        return getAll(StudentPhoto.TABLE_NAME);
    }

    @Override
    public List<StudentPhoto> parseResult(ResultSet resultSet) throws SQLException {
        final List<StudentPhoto> studentPhotos = new ArrayList<>();

        try {
            while (resultSet.next()) {
                final String id = resultSet.getString(StudentPhoto.ID);
                final Blob image = resultSet.getBlob(StudentPhoto.IMAGE);
                final ImageType imageType = new ImageTypeService().get(resultSet.getString(StudentPhoto.IMAGE_TYPE_ID));
                final int imageSize = resultSet.getInt(StudentPhoto.IMAGE_SIZE);
                final String imageName = resultSet.getString(StudentPhoto.IMAGE_NAME);

                final Timestamp created = resultSet.getTimestamp(StudentPhoto.CREATED);
                final Timestamp modified = resultSet.getTimestamp(StudentPhoto.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(StudentPhoto.DELETED);

                studentPhotos.add(new StudentPhoto(id, image, imageType, imageSize, imageName, created, modified, deleted));
            }
        }finally {
            resultSet.close();
        }

        return studentPhotos;
    }
}
