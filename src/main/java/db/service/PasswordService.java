package db.service;

import db.model.Password;
import db.model.core.ITable;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class PasswordService extends AService<Password> {

    @Override
    public Password get(String id) throws SQLException {
        return get(Password.TABLE_NAME, id);
    }

    @Override
    public List<Password> getAll() throws SQLException {
        return getAll(Password.TABLE_NAME);
    }

    @Override
    public List<Password> parseResult(ResultSet resultSet) throws SQLException {
        final List<Password> passwords = new ArrayList<>();

        try {
            while (resultSet.next()){
                final String id = resultSet.getString(ITable.ID);
                final String password = resultSet.getString(Password.PASSWORD);

                final Timestamp created = resultSet.getTimestamp(ITable.CREATED);
                final Timestamp modified = resultSet.getTimestamp(ITable.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(ITable.DELETED);

                passwords.add(new Password(id, password, created, modified, deleted));
            }
        }finally {
            resultSet.close();
        }

        return passwords;
    }
}
