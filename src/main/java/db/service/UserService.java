package db.service;

import db.model.Password;
import db.model.Role;
import db.model.User;
import db.model.core.ITable;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class UserService extends AService<User> {

    @Override
    public User get(String id) throws SQLException {
        return get(User.TABLE_NAME, id);
    }

    @Override
    public List<User> getAll() throws SQLException {
        return getAll(User.TABLE_NAME);
    }

    @Override
    public List<User> parseResult(ResultSet resultSet) throws SQLException {
        final List<User> users = new ArrayList<>();

        try {
            while (resultSet.next()){
                final String id = resultSet.getString(ITable.ID);
                final String userName = resultSet.getString(User.USER_NAME);

                final Password password = new PasswordService().get(resultSet.getString(User.ID_PASSWORD));
                final Role role = new RoleService().get(resultSet.getString(User.USER_ROLE_ID));

                final Timestamp created = resultSet.getTimestamp(ITable.CREATED);
                final Timestamp modified = resultSet.getTimestamp(ITable.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(ITable.DELETED);

                users.add(new User(id, userName, password, role, created, modified, deleted));
            }
        }finally {
            resultSet.close();
        }

        return users;
    }
}
