package db.service;

import com.sun.istack.internal.NotNull;
import db.model.Grade;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class GradeService extends AService<Grade> {

    @Override
    public Grade get(@NotNull String id) throws SQLException {
        return get(Grade.TABLE_NAME, id);
    }

    @Override
    public List<Grade> getAll() throws SQLException {
        return getAll(Grade.TABLE_NAME);
    }

    @Override
    public List<Grade> parseResult(ResultSet resultSet) throws SQLException {
        final List<Grade> grades = new ArrayList<>();

        try {
            while (resultSet.next()) {
                final String id = resultSet.getString(Grade.ID);
                final String gradeName = resultSet.getString(Grade.GRADE_NAME);

                final Timestamp created = resultSet.getTimestamp(Grade.CREATED);
                final Timestamp modified = resultSet.getTimestamp(Grade.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(Grade.DELETED);

                final Grade grade = new Grade(id, gradeName, created, modified, deleted);

                grades.add(grade);
            }
        }finally {
            resultSet.close();
        }

        return grades;
    }
}
