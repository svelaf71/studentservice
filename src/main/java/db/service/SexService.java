package db.service;

import com.sun.istack.internal.NotNull;
import db.model.Sex;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class SexService extends AService<Sex> {

    @Override
    public Sex get(@NotNull String id) throws SQLException {
        return get(Sex.TABLE_NAME, id);
    }

    @Override
    public List<Sex> getAll() throws SQLException {
        return getAll(Sex.TABLE_NAME);
    }

    @Override
    public List<Sex> parseResult(ResultSet resultSet) throws SQLException {
        final List<Sex> sexList = new ArrayList<>();

        try {
            while (resultSet.next()){
                final String id = resultSet.getString(Sex.ID);
                final String sex = resultSet.getString(Sex.SEX);

                final Timestamp created = resultSet.getTimestamp(Sex.CREATED);
                final Timestamp modified = resultSet.getTimestamp(Sex.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(Sex.DELETED);

                sexList.add(new Sex(id, sex, created, modified, deleted));
            }
        }finally {
            resultSet.close();
        }


        return sexList;
    }
}
