package db.service;

import db.model.Group;
import db.model.Nationality;
import db.model.Sex;
import db.model.Status;
import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;
import java.util.Date;

@XmlRootElement(name ="Student")
public class Student extends ATable {
    public static final String TABLE_NAME = "tbl_student";
    public static final String UCODE = "ucode";
    public static final String INTERNAL_ID = "internal_id";
    public static final String GROUP_ID = "group_id";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String PATRONYMIC = "patronymic";
    public static final String DATE_OF_BIRTH = "date_of_birth";
    public static final String SEX_ID = "sex_id";
    public static final String NATIONALITY_ID = "nationality_id";
    public static final String PASSPORT_SERIES = "passport_series";
    public static final String PASSPORT_NUMBER = "passport_number";
    public static final String ID_CODE = "id_code";
    public static final String MAIN_PHONE = "main_phone";
    public static final String OTHER_PHONES = "other_phones";
    public static final String EMAIL = "email";
    public static final String ADDRESS = "address";
    public static final String SOCIAL = "social";
    public static final String FATHER_NAME = "father_name";
    public static final String FATHER_WORK = "father_work";
    public static final String FATHER_PHONE = "father_phone";
    public static final String MOTHER_NAME = "mother_name";
    public static final String MOTHER_WORK = "mother_work";
    public static final String MOTHER_PHONE = "mother_phone";
    public static final String STATUS_ID = "status_id";
    public static final String HOSTEL_ROOM_NUMBER = "hostel_room_number";

    private int ucode;
    private String internalId;
    private Group group;
    private String firstName;
    private String lastName;
    private String patronymic;
    private Date dateOfBirth;
    private Sex sex;
    private Nationality nationality;
    private String passportSeries;
    private String passportNumber;
    private int idCode;
    private String mainPhone;
    private String otherPhones;
    private String email;
    private String address;
    private String social;
    private String fatherName;
    private String fatherWork;
    private String fatherPhone;
    private String motherName;
    private String motherWork;
    private String motherPhone;
    private Status status;
    private int hostelRoomNumber;

    public Student() {}

    public Student(int ucode, String internalId, Group group, String firstName, String lastName, String patronymic, Date dateOfBirth, Sex sex, Nationality nationality, String passportSeries, String passportNumber, int idCode, String mainPhone, String otherPhones, String email, String address, String social, String fatherName, String fatherWork, String fatherPhone, String motherName, String motherWork, String motherPhone, Status status, int hostelRoomNumber) {
        this.ucode = ucode;
        this.internalId = internalId;
        this.group = group;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.dateOfBirth = dateOfBirth;
        this.sex = sex;
        this.nationality = nationality;
        this.passportSeries = passportSeries;
        this.passportNumber = passportNumber;
        this.idCode = idCode;
        this.mainPhone = mainPhone;
        this.otherPhones = otherPhones;
        this.email = email;
        this.address = address;
        this.social = social;
        this.fatherName = fatherName;
        this.fatherWork = fatherWork;
        this.fatherPhone = fatherPhone;
        this.motherName = motherName;
        this.motherWork = motherWork;
        this.motherPhone = motherPhone;
        this.status = status;
        this.hostelRoomNumber = hostelRoomNumber;
    }

    public Student(String id, int ucode, String internalId, Group group, String firstName, String lastName, String patronymic, Date dateOfBirth, Sex sex, Nationality nationality, String passportSeries, String passportNumber, int idCode, String mainPhone, String otherPhones, String email, String address, String social, String fatherName, String fatherWork, String fatherPhone, String motherName, String motherWork, String motherPhone, Status status, int hostelRoomNumber, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.ucode = ucode;
        this.internalId = internalId;
        this.group = group;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.dateOfBirth = dateOfBirth;
        this.sex = sex;
        this.nationality = nationality;
        this.passportSeries = passportSeries;
        this.passportNumber = passportNumber;
        this.idCode = idCode;
        this.mainPhone = mainPhone;
        this.otherPhones = otherPhones;
        this.email = email;
        this.address = address;
        this.social = social;
        this.fatherName = fatherName;
        this.fatherWork = fatherWork;
        this.fatherPhone = fatherPhone;
        this.motherName = motherName;
        this.motherWork = motherWork;
        this.motherPhone = motherPhone;
        this.status = status;
        this.hostelRoomNumber = hostelRoomNumber;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery() {
        return "(" + ID + ", " + UCODE + ", " + INTERNAL_ID + ", " + GROUP_ID + ", " + FIRST_NAME + ", " +
                LAST_NAME + ", " + PATRONYMIC + ", " + DATE_OF_BIRTH + ", " + SEX_ID + ", " + NATIONALITY_ID + ", " +
                PASSPORT_SERIES + ", " + PASSPORT_NUMBER + ", " + ID_CODE + ", " + MAIN_PHONE + ", " +
                OTHER_PHONES + ", " + EMAIL + ", " + ADDRESS + ", " + SOCIAL + ", " + FATHER_NAME + ", " +
                FATHER_WORK + ", " + FATHER_PHONE + ", " + MOTHER_NAME + ", " + MOTHER_WORK + ", " +
                MOTHER_PHONE + ", " + STATUS_ID + ", " + HOSTEL_ROOM_NUMBER + ") VALUES('" +
                getId() + "', " + getUcode() + ", '" + getInternalId() + "', '" + getGroup().getId() + "', '" +
                getFirstName() + "', '" + getLastName() + "', '" + getPatronymic() + "', '" + getDateOfBirth() + "', '" +
                getSex().getId() + "', '" + getNationality().getId() + "', '" + getPassportSeries() + "', '" +
                getPassportNumber() + "', " + getIdCode() + ", '" + getMainPhone() + "', '" + getOtherPhones() + "', '" +
                getEmail() + "', '" + getAddress() + "', '" + getSocial() + "', '" + getFatherName() + "', '" +
                getFatherWork() + "', '" + getFatherPhone() + "', '" + getMotherName() + "', '" + getMotherWork() + "', '" +
                getMotherPhone() + "', '" + getStatus().getId() + "', " + getHostelRoomNumber() + ")";
    }

    @Override
    public String updateQuery() {
        return UCODE + " = " + getUcode() + ", " + INTERNAL_ID + " = '" + getInternalId() + "', " + GROUP_ID + " = '" +
                getGroup().getId() + "', " + FIRST_NAME + " = '" + getFirstName() + "', " + LAST_NAME + " = '" +
                getLastName() + "', " + PATRONYMIC + " = '" + getPatronymic() + "', " + DATE_OF_BIRTH + " = '" +
                getDateOfBirth() + "', " + SEX_ID + " = '" + getSex().getId() + "', " + NATIONALITY_ID + " = '" +
                getNationality().getId() + "', " + PASSPORT_SERIES + " = '" + getPassportSeries() + "', " +
                PASSPORT_NUMBER + " = '" + getPassportNumber() + "', " + ID_CODE + " = " + getIdCode() + ", " +
                MAIN_PHONE + " = '" + getMainPhone() + "', " + OTHER_PHONES + " = '" + getOtherPhones() + "', " +
                EMAIL + " = '" + getEmail() + "', " + ADDRESS + " = '" + getAddress() + "', " + SOCIAL + " = '" +
                getSocial() + "', " + FATHER_NAME + " = '" + getFatherName() + "', " + FATHER_WORK + " = '" +
                getFatherWork() + "', " + FATHER_PHONE + " = '" + getFatherPhone() + "', " + MOTHER_NAME + " = '" +
                getMotherName() + "', " + MOTHER_WORK + " = '" + getMotherWork() + "', " + MOTHER_PHONE + " = '" +
                getMotherPhone() + "', " + STATUS_ID + " = '" + getStatus().getId() + "', " + HOSTEL_ROOM_NUMBER + " = " +
                getHostelRoomNumber();
    }

    public int getUcode() {
        return ucode;
    }

    public void setUcode(int ucode) {
        this.ucode = ucode;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public void setNationality(Nationality nationality) {
        this.nationality = nationality;
    }

    public String getPassportSeries() {
        return passportSeries;
    }

    public void setPassportSeries(String passportSeries) {
        this.passportSeries = passportSeries;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public int getIdCode() {
        return idCode;
    }

    public void setIdCode(int idCode) {
        this.idCode = idCode;
    }

    public String getMainPhone() {
        return mainPhone;
    }

    public void setMainPhone(String mainPhone) {
        this.mainPhone = mainPhone;
    }

    public String getOtherPhones() {
        return otherPhones;
    }

    public void setOtherPhones(String otherPhones) {
        this.otherPhones = otherPhones;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSocial() {
        return social;
    }

    public void setSocial(String social) {
        this.social = social;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getFatherWork() {
        return fatherWork;
    }

    public void setFatherWork(String fatherWork) {
        this.fatherWork = fatherWork;
    }

    public String getFatherPhone() {
        return fatherPhone;
    }

    public void setFatherPhone(String fatherPhone) {
        this.fatherPhone = fatherPhone;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getMotherWork() {
        return motherWork;
    }

    public void setMotherWork(String motherWork) {
        this.motherWork = motherWork;
    }

    public String getMotherPhone() {
        return motherPhone;
    }

    public void setMotherPhone(String motherPhone) {
        this.motherPhone = motherPhone;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getHostelRoomNumber() {
        return hostelRoomNumber;
    }

    public void setHostelRoomNumber(int hostelRoomNumber) {
        this.hostelRoomNumber = hostelRoomNumber;
    }
}
