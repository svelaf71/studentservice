package db.service;

import db.model.Nationality;
import db.model.core.ITable;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class NationalityService extends AService<Nationality> {

    @Override
    public Nationality get(String id) throws SQLException {
        return get(Nationality.TABLE_NAME, id);
    }

    @Override
    public List<Nationality> getAll() throws SQLException {
        return getAll(Nationality.TABLE_NAME);
    }

    @Override
    public List<Nationality> parseResult(ResultSet resultSet) throws SQLException {
        final List<Nationality> nationalities = new ArrayList<>();

        try {
            while (resultSet.next()){
                final String id = resultSet.getString(ITable.ID);
                final String nationality = resultSet.getString(Nationality.NATIONALITY);

                final Timestamp created = resultSet.getTimestamp(ITable.CREATED);
                final Timestamp modified = resultSet.getTimestamp(ITable.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(ITable.DELETED);

                nationalities.add(new Nationality(id, nationality, created, modified, deleted));
            }
        }finally {
            resultSet.close();
        }

        return nationalities;
    }
}
