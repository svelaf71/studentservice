package db.service;

import com.sun.istack.internal.NotNull;
import db.model.Group;
import db.model.Nationality;
import db.model.Sex;
import db.model.Status;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StudentService extends AService<Student> {
    @Override
    public Student get(@NotNull String id) throws SQLException {
        return get(Student.TABLE_NAME, id);
    }

    @Override
    public List<Student> getAll() throws SQLException {
        return getAll(Student.TABLE_NAME);
    }

    @Override
    public List<Student> parseResult(ResultSet resultSet) throws SQLException {
        final List<Student> students = new ArrayList<>();

        try {
            while (resultSet.next()) {
                final String id = resultSet.getString(Student.ID);
                final int ucode = resultSet.getInt(Student.UCODE);
                final String internalId = resultSet.getString(Student.INTERNAL_ID);
                final Group group = new GroupService().get(resultSet.getString(Student.GROUP_ID));
                final String firstName = resultSet.getString(Student.FIRST_NAME);
                final String lastName = resultSet.getString(Student.LAST_NAME);
                final String patronymic = resultSet.getString(Student.PATRONYMIC);
                final Date dateOfBirth = resultSet.getDate(Student.DATE_OF_BIRTH);
                final Sex sex = new SexService().get(resultSet.getString(Student.SEX_ID));
                final Nationality nationality = new NationalityService().get(resultSet.getString(Student.NATIONALITY_ID));
                final String passportSeries = resultSet.getString(Student.PASSPORT_SERIES);
                final String passportNumber = resultSet.getString(Student.PASSPORT_NUMBER);
                final int idCode = resultSet.getInt(Student.ID_CODE);
                final String mainPhone = resultSet.getString(Student.MAIN_PHONE);
                final String otherPhones = resultSet.getString(Student.OTHER_PHONES);
                final String email = resultSet.getString(Student.EMAIL);
                final String address = resultSet.getString(Student.ADDRESS);
                final String social = resultSet.getString(Student.SOCIAL);
                final String fatherName = resultSet.getString(Student.FATHER_NAME);
                final String fatherWork = resultSet.getString(Student.FATHER_WORK);
                final String fatherPhone = resultSet.getString(Student.FATHER_PHONE);
                final String motherName = resultSet.getString(Student.MOTHER_NAME);
                final String motherWork = resultSet.getString(Student.MOTHER_WORK);
                final String motherPhone = resultSet.getString(Student.MOTHER_PHONE);
                final Status status = new StatusService().get(resultSet.getString(Student.STATUS_ID));
                final int hostelRoomNumber = resultSet.getInt(Student.HOSTEL_ROOM_NUMBER);

                final Timestamp created = resultSet.getTimestamp(Student.CREATED);
                final Timestamp modified = resultSet.getTimestamp(Student.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(Student.DELETED);

                students.add(new Student(id, ucode, internalId, group, firstName, lastName, patronymic, dateOfBirth, sex,
                        nationality, passportSeries, passportNumber, idCode, mainPhone, otherPhones, email, address, social,
                        fatherName, fatherWork, fatherPhone, motherName, motherWork, motherPhone, status, hostelRoomNumber,
                        created, modified, deleted));
            }
        }finally {
            resultSet.close();
        }

        return students;
    }
}
