package db.service;

import com.sun.istack.internal.NotNull;
import db.model.Institute;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class InstituteService extends AService<Institute> {

    @Override
    public Institute get(@NotNull String id) throws SQLException {
        return get(Institute.TABLE_NAME, id);
    }

    @Override
    public List<Institute> getAll() throws SQLException {
        return getAll(Institute.TABLE_NAME);
    }

    @Override
    public List<Institute> parseResult(ResultSet resultSet) throws SQLException {
        final List<Institute> institutes = new ArrayList<>();

        try {
            while (resultSet.next()) {
                final String id = resultSet.getString(Institute.ID);
                final String instituteName = resultSet.getString(Institute.INSTITUTE_NAME);

                final Timestamp created = resultSet.getTimestamp(Institute.CREATED);
                final Timestamp modified = resultSet.getTimestamp(Institute.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(Institute.DELETED);

                final Institute institute = new Institute(id, instituteName, created, modified, deleted);

                institutes.add(institute);
            }
        }finally {
            resultSet.close();
        }

        return institutes;
    }
}
