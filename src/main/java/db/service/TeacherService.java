package db.service;

import com.sun.istack.internal.NotNull;
import db.model.Department;
import db.model.Teacher;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TeacherService extends AService<Teacher> {
    @Override
    public Teacher get(@NotNull String id) throws SQLException {
        return get(Teacher.TABLE_NAME, id);
    }

    @Override
    public List<Teacher> getAll() throws SQLException {
        return getAll(Teacher.TABLE_NAME);
    }

    @Override
    public List<Teacher> parseResult(ResultSet resultSet) throws SQLException {
        final List<Teacher> teachers = new ArrayList<>();

        try {
            while (resultSet.next()) {
                final String id = resultSet.getString(Teacher.ID);
                final Department department = new DepartmentService().get(resultSet.getString(Teacher.DEPARTMENT_ID));
                final String firstName = resultSet.getString(Teacher.FIRST_NAME);
                final String lastName = resultSet.getString(Teacher.LAST_NAME);
                final String patronymic = resultSet.getString(Teacher.PATRONYMIC);

                final Timestamp created = resultSet.getTimestamp(Teacher.CREATED);
                final Timestamp modified = resultSet.getTimestamp(Teacher.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(Teacher.DELETED);

                teachers.add(new Teacher(id, department, firstName, lastName, patronymic, created, modified, deleted));
            }
        }finally {
            resultSet.close();
        }

        return teachers;
    }
}
