package db.service;

import com.sun.istack.internal.NotNull;
import db.model.Department;
import db.model.Institute;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class DepartmentService extends AService<Department> {

    @Override
    public Department get(@NotNull String id) throws SQLException {
        return get(Department.TABLE_NAME, id);
    }

    @Override
    public List<Department> getAll() throws SQLException {
        return getAll(Department.TABLE_NAME);
    }

    @Override
    public List<Department> parseResult(ResultSet resultSet) throws SQLException {
        final List<Department> departments = new ArrayList<>();

        try {
            while (resultSet.next()) {
                final String id = resultSet.getString(Department.ID);
                final String departmentName = resultSet.getString(Department.DEPARTMENT_NAME);
                final Institute instituteId = new InstituteService().get(resultSet.getString(Department.INSTITUTE_ID));

                final Timestamp created = resultSet.getTimestamp(Department.CREATED);
                final Timestamp modified = resultSet.getTimestamp(Department.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(Department.DELETED);

                final Department department = new Department(id, departmentName, instituteId, created, modified, deleted);

                departments.add(department);
            }
        }finally {
            resultSet.close();
        }

        return departments;
    }
}
