package db.service;

import com.sun.istack.internal.NotNull;
import db.model.CuratorTeacher;
import db.model.Group;
import db.model.Teacher;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class CuratorTeacherService extends AService<CuratorTeacher> {

    @Override
    public CuratorTeacher get(@NotNull String id) throws SQLException {
        return get(CuratorTeacher.TABLE_NAME, id);
    }

    @Override
    public List<CuratorTeacher> getAll() throws SQLException {
        return getAll(CuratorTeacher.TABLE_NAME);
    }

    @Override
    public List<CuratorTeacher> parseResult(ResultSet resultSet) throws SQLException {
        final List<CuratorTeacher> curatorTeachers = new ArrayList<>();

        try {
            while (resultSet.next()) {
                final String id = resultSet.getString(CuratorTeacher.ID);
                final Teacher teacher = new TeacherService().get(resultSet.getString(CuratorTeacher.TEACHER_ID));
                final Group group = new GroupService().get(resultSet.getString(CuratorTeacher.GROUP_ID));

                final Timestamp created = resultSet.getTimestamp(CuratorTeacher.CREATED);
                final Timestamp modified = resultSet.getTimestamp(CuratorTeacher.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(CuratorTeacher.DELETED);

                final CuratorTeacher curatorTeacher = new CuratorTeacher(id, teacher, group, created, modified, deleted);

                curatorTeachers.add(curatorTeacher);
            }
        }finally{
            resultSet.close();
        }

        return curatorTeachers;
    }
}
