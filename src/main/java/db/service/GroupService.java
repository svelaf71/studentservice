package db.service;

import db.model.Group;
import db.model.Level;
import db.model.core.ITable;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class GroupService extends AService<Group> {

    @Override
    public Group get(String id) throws SQLException {
        return get(Group.TABLE_NAME, id);
    }

    @Override
    public List<Group> getAll() throws SQLException {
        return getAll(Group.TABLE_NAME);
    }

    @Override
    public List<Group> parseResult(ResultSet resultSet) throws SQLException {
        final List<Group> groups = new ArrayList<>();

        try {
            while (resultSet.next()){
                final String id = resultSet.getString(ITable.ID);
                final String groupName = resultSet.getString(Group.GROUP_NAME);

                final Timestamp created = resultSet.getTimestamp(ITable.CREATED);
                final Timestamp modified = resultSet.getTimestamp(ITable.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(ITable.DELETED);

                final Level level = new LevelService().get(resultSet.getString(Group.LEVEL_ID));

                groups.add(new Group(id, groupName, level, created, modified, deleted));
            }
        }finally {
            resultSet.close();
        }

        return groups;
    }
}
