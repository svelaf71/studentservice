package db.service;

import com.sun.istack.internal.NotNull;
import db.model.Status;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class StatusService extends AService<Status> {
    @Override
    public Status get(@NotNull String id) throws SQLException {
        return get(Status.TABLE_NAME);
    }

    @Override
    public List<Status> getAll() throws SQLException {
        return getAll(Status.TABLE_NAME);
    }

    @Override
    public List<Status> parseResult(ResultSet resultSet) throws SQLException {
        final List<Status> statuses = new ArrayList<>();

        try {
            while (resultSet.next()) {
                final String id = resultSet.getString(Status.ID);
                final String statusName = resultSet.getString(Status.STATUS_NAME);

                final Timestamp created = resultSet.getTimestamp(Status.CREATED);
                final Timestamp modified = resultSet.getTimestamp(Status.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(Status.DELETED);

                statuses.add(new Status(id, statusName, created, modified, deleted));
            }
        }finally {
            resultSet.close();
        }

        return statuses;
    }
}
