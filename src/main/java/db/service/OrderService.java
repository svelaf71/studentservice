package db.service;

import com.sun.istack.internal.NotNull;
import db.model.Order;
import db.service.core.AService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class OrderService extends AService<Order> {
    @Override
    public Order get(@NotNull String id) throws SQLException {
        return get(Order.TABLE_NAME, id);
    }

    @Override
    public List<Order> getAll() throws SQLException {
        return getAll(Order.TABLE_NAME);
    }

    @Override
    public List<Order> parseResult(ResultSet resultSet) throws SQLException {
        final List<Order> orders = new ArrayList<>();

        try {
            while (resultSet.next()) {
                final String id = resultSet.getString(Order.ID);
                final String orderNumber = resultSet.getString(Order.ORDER_NUMBER);

                final Timestamp created = resultSet.getTimestamp(Order.CREATED);
                final Timestamp modified = resultSet.getTimestamp(Order.MODIFIED);
                final Timestamp deleted = resultSet.getTimestamp(Order.DELETED);

                orders.add(new Order(id, orderNumber, created, modified, deleted));
            }
        }finally {
            resultSet.close();
        }

        return orders;
    }
}
