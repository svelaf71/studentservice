package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "Password")
public class Password extends ATable {
    public static final String TABLE_NAME = "tbl_pass";
    public static final String PASSWORD = "password";

    private String password;

    public Password() {}

    public Password(String password) {
        this.password = password;
    }

    public Password(String id, String password, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.password = password;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery(){
        return "(" + ID + ", " + PASSWORD + ") VALUES('" + getId() + "', '" + getPassword() + "')";
    }

    @Override
    public String updateQuery() {
        return PASSWORD + " = '" + getPassword() + "'";
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
