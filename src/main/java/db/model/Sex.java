package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "Sex")
public class Sex extends ATable {
    public static final String TABLE_NAME = "tbl_sex";
    public static final String SEX = "sex";

    private String sex;

    public Sex() {}

    public Sex(String sex) {
        this.sex = sex;
    }

    public Sex(String id, String sex, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.sex = sex;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery() {
        return "(" + ID + ", " + SEX + ") VALUES('" + getId() + "', '" + getSex() + "')";
    }

    @Override
    public String updateQuery() {
        return SEX + " = '" + getSex() + "'";
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
