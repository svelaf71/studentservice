package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "Group")
public class Group extends ATable {
    public static final String TABLE_NAME = "tbl_group";
    public static final String GROUP_NAME = "group_name";
    public static final String LEVEL_ID = "level_id";

    private String groupName;
    private Level level;

    public Group() {}

    public Group(String groupName, Level level) {
        this.groupName = groupName;
        this.level = level;
    }

    public Group(String id, String groupName, Level level, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.groupName = groupName;
        this.level = level;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery(){
        return "(" + ID + ", " + GROUP_NAME + ", " + LEVEL_ID + ") VALUES('" + getId() + "', '" + getGroupName() + "', '"
                + getLevel().getId() + "')";
    }

    @Override
    public String updateQuery() {
        return GROUP_NAME + " = '" + getGroupName() + "', " + LEVEL_ID + " = '" + getLevel().getId() + "'";
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }
}
