package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "ImageType")
public class ImageType extends ATable {
    public static final String TABLE_NAME = "tbl_image_type";
    public static final String IMAGE_TYPE = "image_type";

    private String imageType;

    public ImageType() {}

    public ImageType(String imageType) {
        this.imageType = imageType;
    }

    public ImageType(String id, String imageType, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.imageType = imageType;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery() {
        return "(" + ID + ", " + IMAGE_TYPE + ") VALUES('" +
                getId() + "', '" + getImageType() + "')";
    }

    @Override
    public String updateQuery() {
        return IMAGE_TYPE + " = '" + getImageType() + "'";
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }
}
