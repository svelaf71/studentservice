package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "Level")
public class Level extends ATable {
    public static final String TABLE_NAME = "tbl_level";
    public static final String LEVEL_NAME = "level_name";

    private int levelName;

    public Level() {}

    public Level(int levelName) {
        this.levelName = levelName;
    }

    public Level(String id, int levelName, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.levelName = levelName;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery(){
        return "(" + ID + ", " + LEVEL_NAME + ") VALUES('" + getId() + "', '" + getLevelName() + "')";
    }

    @Override
    public String updateQuery() {
        return LEVEL_NAME + " = '" + getLevelName() + "'";
    }

    public int getLevelName() {
        return levelName;
    }

    public void setLevelName(int levelName) {
        this.levelName = levelName;
    }
}
