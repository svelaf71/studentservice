package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Blob;
import java.sql.Timestamp;

@XmlRootElement(name = "StudentPhoto")
public class StudentPhoto extends ATable {
    public static final String TABLE_NAME = "tbl_status";
    public static final String IMAGE = "image";
    public static final String IMAGE_TYPE_ID = "image_type_id";
    public static final String IMAGE_SIZE = "image_size";
    public static final String IMAGE_NAME = "image_name";

    private Blob image;
    private ImageType imageType;
    private int imageSize;
    private String imageName;

    public StudentPhoto() {}

    public StudentPhoto(Blob image, ImageType imageType, int imageSize, String imageName) {
        this.image = image;
        this.imageType = imageType;
        this.imageSize = imageSize;
        this.imageName = imageName;
    }

    public StudentPhoto(String id, Blob image, ImageType imageType, int imageSize, String imageName, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.image = image;
        this.imageType = imageType;
        this.imageSize = imageSize;
        this.imageName = imageName;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery() {
        return "(" + ID + ", " + IMAGE + ", " + IMAGE_TYPE_ID + ", " + IMAGE_SIZE + ", " + IMAGE_NAME + ") VALUES('" +
                getId() + "', " + getImage() + ", '" + getImageType().getId() + "', " + getImageSize() + ", '" + getImageName() + "')";
    }

    @Override
    public String updateQuery() {
        return IMAGE + " = " + getImage() + ", " + IMAGE_TYPE_ID + " = '" + getImageType().getId() + "'" + ", " +
                IMAGE_SIZE + " = " + getImageSize() + ", " + IMAGE_NAME + " = '" + getImageName() + "'";
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }

    public ImageType getImageType() {
        return imageType;
    }

    public void setImageType(ImageType imageType) {
        this.imageType = imageType;
    }

    public int getImageSize() {
        return imageSize;
    }

    public void setImageSize(int imageSize) {
        this.imageSize = imageSize;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
