package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "Status")
public class Status extends ATable {
    public static final String TABLE_NAME = "tbl_status";
    public static final String STATUS_NAME = "status_name";

    private String statusName;

    public Status() {}

    public Status(String statusName) {
        this.statusName = statusName;
    }

    public Status(String id, String statusName, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.statusName = statusName;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery() {
        return "(" + ID + ", " + STATUS_NAME + ") VALUES('" +
                getId() + "', '" + getStatusName() + "')";
    }

    @Override
    public String updateQuery() {
        return STATUS_NAME + " = '" + getStatusName() + "'";
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
