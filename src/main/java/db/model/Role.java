package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "Role")
public class Role extends ATable {
    public static final String TABLE_NAME = "tbl_role";
    public static final String ROLE_NAME = "role_name";

    private String roleName;

    public Role() {}

    public Role(String roleName) {
        this.roleName = roleName;
    }

    public Role(String id, String roleName, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.roleName = roleName;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery(){
        return "(" + ID + ", " + ROLE_NAME + ") VALUES('" + getId() + "', '" + getRoleName() + "')";
    }

    @Override
    public String updateQuery() {
        return ROLE_NAME + " = '" + getRoleName() + "'";
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
