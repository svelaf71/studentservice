package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "Order")
public class Order extends ATable {
    public static final String TABLE_NAME = "tbl_order";
    public static final String ORDER_NUMBER = "order_number";

    private String orderNumber;

    public Order() {}

    public Order(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Order(String id, String orderNumber, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.orderNumber = orderNumber;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery() {
        return "(" + ID + ", " + ORDER_NUMBER + ") VALUES('" +
                getId() + "', '" + getOrderNumber() + "')";
    }

    @Override
    public String updateQuery() {
        return ORDER_NUMBER + " = '" + getOrderNumber() + "'";
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }
}
