package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "Form")
public class Form extends ATable {
    public static final String TABLE_NAME = "tbl_form";
    public static final String FORM_NAME = "form_name";

    private String formName;

    public Form() {}

    public Form(String formName) {
        this.formName = formName;
    }

    public Form(String id, String formName, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.formName = formName;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery() {
        return "(" + ID + ", " + FORM_NAME + ") VALUES('" + getId() + "', '" + getFormName() + "')";
    }

    @Override
    public String updateQuery() {
        return FORM_NAME + " = '" + getFormName() + "', " +
                MODIFIED + " = '" + new Timestamp(System.currentTimeMillis()) + "'";
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }
}
