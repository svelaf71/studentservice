package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "Nationality")
public class Nationality extends ATable {
    public static final String TABLE_NAME = "tbl_nationality";
    public static final String NATIONALITY = "nationality";

    private String nationality;

    public Nationality() {}

    public Nationality(String nationality) {
        this.nationality = nationality;
    }

    public Nationality(String id, String nationality, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.nationality = nationality;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery(){
        return "(" + ID + ", " + NATIONALITY + ") VALUES('" + getId() + "', '" + getNationality() + "')";
    }

    @Override
    public String updateQuery() {
        return NATIONALITY + " = '" + getNationality() + "'";
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
}
