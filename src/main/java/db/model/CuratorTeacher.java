package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "CuratorTeacher")
public class CuratorTeacher extends ATable {
    public static final String TABLE_NAME = "tbl_curator_teacher";
    public static final String TEACHER_ID = "teacher_id";
    public static final String GROUP_ID = "group_id";

    private Teacher teacher;
    private Group group;

    public CuratorTeacher() {}

    public CuratorTeacher(Teacher teacher, Group group) {
        this.teacher = teacher;
        this.group = group;
    }

    public CuratorTeacher(String id, Teacher teacher, Group group, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.teacher = teacher;
        this.group = group;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery() {
        return "(" + ID + ", " + TEACHER_ID + ", " + GROUP_ID + ") VALUES('" +
                getId() + "', '" + getTeacher().getId() + "', '" + getGroup().getId() + "')";
    }

    @Override
    public String updateQuery() {
        return TEACHER_ID + " = '" + getTeacher().getId() + "', " + GROUP_ID + " = '" + getGroup().getId() + "'";
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
