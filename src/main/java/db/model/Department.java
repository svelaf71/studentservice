package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "Department")
public class Department extends ATable {
    public static final String TABLE_NAME = "tbl_department";
    public static final String DEPARTMENT_NAME = "department_name";
    public static final String INSTITUTE_ID = "institute_id";

    private String departmentName;
    private Institute institute;

    public Department() {}

    public Department(String departmentName, Institute institute) {
        this.departmentName = departmentName;
        this.institute = institute;
    }

    public Department(String id, String departmentName, Institute institute,
                      Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.departmentName = departmentName;
        this.institute = institute;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery() {
        return "(" + ID + ", " + DEPARTMENT_NAME + ", " + INSTITUTE_ID + ") VALUES('" +
                getId() + "', '" + getDepartmentName() + "', '" + getInstitute().getId() + "')";
    }

    @Override
    public String updateQuery() {
        return DEPARTMENT_NAME + " = '" + getDepartmentName() + "', " + INSTITUTE_ID + " = '" +
                getInstitute().getId() + "'";
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Institute getInstitute() {
        return institute;
    }

    public void setInstitute(Institute institute) {
        this.institute = institute;
    }
}
