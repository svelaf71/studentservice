package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "Institute")
public class Institute extends ATable {
    public static final String TABLE_NAME = "tbl_institute";
    public static final String INSTITUTE_NAME = "institute_name";

    private String instituteName;

    public Institute() {}

    public Institute(String instituteName) {
        this.instituteName = instituteName;
    }

    public Institute(String id, String instituteName, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.instituteName = instituteName;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery() {
        return "(" + ID + ", " + INSTITUTE_NAME + ") VALUES('" +
                getId() + "', '" + getInstituteName() + "')";
    }

    @Override
    public String updateQuery() {
        return INSTITUTE_NAME + " = '" + getInstituteName() + "'";
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }
}
