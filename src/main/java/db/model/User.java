package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "User")
public class User extends ATable {
    public static final String TABLE_NAME = "tbl_user";
    public static final String USER_NAME = "user_name";
    public static final String USER_ROLE_ID = "user_role_id";
    public static final String ID_PASSWORD = "id_password";

    private String userName;
    private Password password;
    private Role role ;

    public User() {}

    public User(String userName, Password password, Role role) {
        this.userName = userName;
        this.password = password;
        this.role = role;
    }

    public User(String id, String userName, Password password, Role role,
                Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.userName = userName;
        this.password = password;
        this.role = role;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery(){
        return "(" + ID + ", " + USER_NAME + ", " + USER_ROLE_ID + ", " + ID_PASSWORD + ") VALUES('" +
                getId() + "', '" + getUserName() + "', '" +  getRole().getId() + "', '" + getPassword().getId() + "')";
    }

    @Override
    public String updateQuery() {
        return USER_NAME + " = '" + getUserName() + "', " +
                USER_ROLE_ID + " = '" + getRole().getId() + "', " +
                ID_PASSWORD + " = '" + getPassword().getId() + "'";
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
