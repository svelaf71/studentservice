package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "Teacher")
public class Teacher extends ATable {
    public static final String TABLE_NAME = "tbl_teacher";
    public static final String DEPARTMENT_ID = "department_id";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String PATRONYMIC = "patronymic";

    private Department department;
    private String firstName;
    private String lastName;
    private String patronymic;

    public Teacher() {}

    public Teacher(Department department, String firstName, String lastName, String patronymic) {
        this.department = department;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
    }

    public Teacher(String id, Department department, String firstName, String lastName, String patronymic, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.department = department;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery() {
        return "(" + ID + ", " + DEPARTMENT_ID + ", " + FIRST_NAME + ", " + LAST_NAME + ", " + PATRONYMIC + ") VALUES('" +
                getId() + "', '" + getDepartment().getId() + "', '" + getFirstName() + "', '" + getLastName() + "', '" + getPatronymic() + "')";
    }

    @Override
    public String updateQuery() {
        return DEPARTMENT_ID + " = '" + getDepartment().getId() + "', " + FIRST_NAME + " = '" + getFirstName() + "', " +
                LAST_NAME + " = '" + getLastName() + "', " + PATRONYMIC + " = '" + getPatronymic() + "'";
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }
}
