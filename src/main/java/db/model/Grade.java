package db.model;

import db.model.core.ATable;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement(name = "Grade")
public class Grade extends ATable {
    public static final String TABLE_NAME = "tbl_grade";
    public static final String GRADE_NAME = "grade_name";

    private String gradeName;

    public Grade() {}

    public Grade(String gradeName) {
        this.gradeName = gradeName;
    }

    public Grade(String id, String gradeName, Timestamp created, Timestamp modified, Timestamp deleted) {
        super(id, created, modified, deleted);
        this.gradeName = gradeName;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String insertQuery() {
        return "(" + ID + ", " + GRADE_NAME + ") VALUES('" +
                getId() + "', '" + getGradeName() + "')";
    }

    @Override
    public String updateQuery() {
        return GRADE_NAME + " = '" + getGradeName() + "', " +
                MODIFIED + " = '" + new Timestamp(System.currentTimeMillis()) + "'";
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }
}
