package db.model.core;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.sql.Timestamp;
import java.util.Date;

@XmlRootElement
@XmlType(propOrder = {"id", "created", "modified", "deleted"})
public abstract class ATable implements ITable {
    private String id;
    private Date created;
    private Date modified;
    private Date deleted;

    public ATable() {}

    public ATable(String id, Timestamp created, Timestamp modified, Timestamp deleted) {
        this.id = id;
        this.created = created;
        this.modified = modified;
        this.deleted = deleted;
    }

    @Override
    @XmlElement(name = "id")
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @XmlElement(name = "created")
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @XmlElement(name = "modified")
    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    @XmlElement(name = "deleted")
    public Date getDeleted() {
        return deleted;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "id: " + id + ", created: " + created + ", modified: " + modified + ", deleted: " + deleted;
    }
}
