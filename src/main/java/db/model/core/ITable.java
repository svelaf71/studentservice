package db.model.core;

import java.util.Date;

/**
 * Every table has: columns(id, created, modified, deleted), table name;
 * And must have insert and update query;
 *
 */
public interface ITable {
    String ID = "id";
    String CREATED = "created";
    String MODIFIED = "modified";
    String DELETED = "deleted";

    String getTableName();

    String getId();

    void setId(String id);

    String insertQuery();

    String updateQuery();

    Date getCreated();

    void setCreated(Date created);

    Date getModified();

    void setModified(Date modified);

    Date getDeleted();

    void setDeleted(Date deleted);
}
